#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass report
\begin_preamble
\newcommand{\simplechapter}[1][\@empty]{%
  \let\@tbiold@makechapterhead\@makechapterhead
  \renewcommand{\@makechapterhead}[1]{%
    \vspace*{50\p@}%
    {\parindent \z@ \raggedright
     \normalfont
     \interlinepenalty\@M
     \Huge\bfseries #1\space\thechapter\simplechapterdelim\space
       ##1\par\nobreak
     \vskip 40\p@
  }}
}

\newcommand{\restorechapter}{%
  \let\@makechapterhead\@tbiold@makechapterhead
}

\newcommand{\simplechapterdelim}{}

\renewcommand\thechapter{\arabic{chapter}.}
\renewcommand\thesection{\thechapter\arabic{section}.}
\renewcommand\thesubsection{\thesection\arabic{subsection}.}
\renewcommand\thesubsubsection{\thesubsection\arabic{subsubsection}.}
\renewcommand\theparagraph{\thesubsubsection\arabic{paragraph}.}
\renewcommand\thesubparagraph{\theparagraph\arabic{subparagraph}.}


\usepackage{remreset}
\makeatletter
% Remove numbering within chapters
\@removefromreset{figure}{chapter}
\renewcommand*{\thefigure}{\arabic{figure}}
\end_preamble
\use_default_options true
\maintain_unincluded_children false
\language british
\language_package default
\inputencoding utf8
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref true
\pdf_bookmarks true
\pdf_bookmarksnumbered false
\pdf_bookmarksopen false
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder false
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle true
\papersize a4paper
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 2.5cm
\topmargin 2.5cm
\rightmargin 2.5cm
\bottommargin 2.5cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language polish
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Spring: 1944 Tutorial
\end_layout

\begin_layout Author
ThinkSome
\end_layout

\begin_layout Standard
\begin_inset CommandInset toc
LatexCommand tableofcontents

\end_inset


\end_layout

\begin_layout Chapter
Resources and Economy
\end_layout

\begin_layout Section
\begin_inset Quotes pld
\end_inset

Command
\begin_inset Quotes prd
\end_inset


\end_layout

\begin_layout Subsection
What it is
\end_layout

\begin_layout Standard
On the HUD
\begin_inset Foot
status open

\begin_layout Plain Layout
heads-up display
\end_layout

\end_inset

, you have two meters: one is white and represents command.
 Command is the most important resource in game.
 You need command for building stuff.
\end_layout

\begin_layout Subsection
Obtaining command
\end_layout

\begin_layout Subsubsection
Flags
\end_layout

\begin_layout Standard
The main command income generating 
\begin_inset Quotes pld
\end_inset

objects
\begin_inset Quotes prd
\end_inset

 in this game are flags
\begin_inset Foot
status open

\begin_layout Plain Layout
you can see a flag waving on the pole when you zoom in
\end_layout

\end_inset

.
 Flags produce command income starting at a map specific value.
 Some produce more, some less.
 Every game minute, the amount given by a flag will increase.
 This will reset after it is captured again (by enemy).
 It is very important not to lose back-end flags, as those usually are held
 for long periods of time and will gradually produce a lot of income (e.g.
 32 on Kiev).
 Those near front line will change hands many times and thus are not important
 enough to lose the game over them.
\end_layout

\begin_layout Subsubsection
\begin_inset Quotes pld
\end_inset

base income
\begin_inset Quotes prd
\end_inset


\end_layout

\begin_layout Standard
Throughout the game, you will receive a certain amount of command, no matter
 what.
 This amount currently equals 25 per second.
\end_layout

\begin_layout Subsubsection
Reclaiming
\end_layout

\begin_layout Standard
You may obtain some percentage (~75%) of command used making a building
 by reclaiming it with engineers.
 It is a good idea to reclaim buildings you do not plan on using anytime
 soon.
 The command will transfer once the reclaim is complete.
\end_layout

\begin_layout Subsubsection
Planes returning
\end_layout

\begin_layout Standard
For most types of planes (interceptors, ground attack, scout, ...) you will
 get most of the command back if they safely return to base (off-map fictional
 base, not your base).
 Additionally, the amount will reduce if the plane is damaged according
 to the following formula:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
amount=\frac{HP_{remaining}}{HP_{total}}*modifier*Costtobuild
\]

\end_inset


\end_layout

\begin_layout Standard
Most of the planes define their own modifier with the default being 65%.
\begin_inset Note Note
status open

\begin_layout Plain Layout
file: LuaRules/Gadgets/game_planes.lua, search for 
\begin_inset Quotes pld
\end_inset

AddTeamResource
\begin_inset Quotes prd
\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Using command
\end_layout

\begin_layout Subsubsection
At start of game
\end_layout

\begin_layout Standard
When you start playing, absolutely do not build 10 engineers
\begin_inset Foot
status open

\begin_layout Plain Layout
unless you *really* know what you are doing
\end_layout

\end_inset

, notable exception being map 
\begin_inset Quotes pld
\end_inset

Kiev
\begin_inset Quotes prd
\end_inset

.
 Managing you economy well is one of the keys to success.
 Every engineer building something drains 15 command.
 It does not matter if you have 10 engineers or 1 engineer building something
 if your income is only 15, the thing will take equal time or more to build
 as you've needlessly spent 3000 command on those 9 engineers, which could
 be better used for building said thing or making more soldiers!.
 Speaking of that, always have the headquarters building producing infantry
 squads, they are the cheapest there! You may want to tick to build one
 of them and then set the headquarters on REPEAT.
 Unlike in *TA, S44 buildings cannot be assisted.
\end_layout

\begin_layout Subsubsection
Informative figures
\end_layout

\begin_layout Standard
Here are the informative command drain figures for the usual stuff
\end_layout

\begin_layout Standard
\begin_inset Tabular
<lyxtabular version="3" rows="10" columns="2">
<features rotate="0" tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Unit/Building
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
command drain when building something
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Engineer
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
15
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Barracks
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
15
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Headquarters
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
20
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
towed gun yard
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
50
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
light vehicle yard
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
30
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
radar (air factory)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
100
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
boat yard
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
75
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
upgraded boat yard
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
100
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
tank depot
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
100
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Section
\begin_inset Quotes pld
\end_inset

Logistics
\begin_inset Quotes prd
\end_inset

 a.k.a.
 ammo
\end_layout

\begin_layout Subsection
What it is
\end_layout

\begin_layout Standard
The other meter on your HUD is yellow and represents ammo.
 you need it for anything bigger than 50 cal gun.
 Note that rifle rounds, AT grenades, ordinary grenades and MG rounds are
 free (they do not consume logistics).
\end_layout

\begin_layout Subsection
Obtaining ammo
\end_layout

\begin_layout Subsubsection
Storage refill
\end_layout

\begin_layout Standard
You get ammo from a building called a 
\begin_inset Quotes pld
\end_inset

supply storage
\begin_inset Quotes prd
\end_inset

, you always get one free storage when you start the game, with soviets
 this is a truck but all other factions have it as a building.
 You can build more storage using engineers or construction vehicles (every
 storage gives ~1000 ammo).
 Storages are refilled every seven and a half minutes, and the time till
 refill is indicated above the yellow bar.
 Never build this building at the frontline.
 It is very fragile, expensive and doesen't directly benefit the fighting,
 you don't want to lose it.
 Instead, build it somewhere behind your base, preferrably as far away from
 the enemy as possible.
 It is also recommended to build some tank trap lines around them and your
 base in general (in a radius of atleast 5 storage sizes) to prevent them
 being tank-rushed.
 Be careful to always put a space the size of two storages between them
 to prevent them blowing up in a chain reaction when one unfortunately gets
 killed (be it from tank rush or infantry or artillery).
 Note that recent changes in the development version of the game have made
 it so that small vehicles (e.g.
 US greyhounds, GBR daimlers, RUS BA64, ...) can sneak through tank barricades,
 so build two lines close together, but shifted.
\end_layout

\begin_layout Subsubsection
Japanese storage upgrade
\end_layout

\begin_layout Standard
Japanese are the only faction in game that can constantly obtain storage.
 If you upgrade their storages into 
\begin_inset Quotes pld
\end_inset

Supply Tunnel Exit
\begin_inset Quotes prd
\end_inset

, it will give you a small amount of storage every second, up to total storage
 capacity.
 
\end_layout

\begin_layout Subsection
Using ammo
\end_layout

\begin_layout Subsubsection
How much does a unit need?
\end_layout

\begin_layout Standard
The ammo usage of a unit can be seen by hovering over it and looking into
 the bottom left corner.
 In the first row you will see a line like "log.
 usage: N, total: M", where N is the usage for ONE SHOT and M is how much
 ammo a unit can carry.
\end_layout

\begin_layout Subsubsection
Delivering ammo to units
\end_layout

\begin_layout Standard
The ammo you have in storage does not automatically transfer to units on
 the field, instead, it is provided by units/buildings called 
\begin_inset Quotes pld
\end_inset

supply providers
\begin_inset Quotes prd
\end_inset

.
 For them to resupply your units (mortars, vehicles, artillery, tanks, ...),
 you need them to be in their supply radius, which is indicated by yellow
 dotted circles around every supply provider (HQ, factories, deployed trucks,
 support halftrack, large supply depot).
 If you hover over one of those, you will see all supply ranges on the map.
 The most commonly used suppliers: 
\end_layout

\begin_layout Itemize
Factories: these are the providers you start with.
 Though they may not seem that important right now, they are essential for
 giving newly-built units their initial ordance load.
 you may want to set your factories (non-infantry) destination to still
 be within range of the supply so they dont run off into battle empty-handed.
\end_layout

\begin_layout Itemize
Deployed trucks: This unit costs about 500 command and can be build at barracks,
 light vehicle factory or towed gun yard (and their upgraded versions).
 Before it can do its job, you have to deploy it (hit the deploy button).
 After it is deployed (it will take roughly 20s), it will supply all units
 in a factory-like radius around it.
 
\end_layout

\begin_layout Itemize
Large supply depot: This building has an extremely long supply range and
 costs about 4000 command.
 You can only build it using a construction vehicle.
 Its main benefit is that you don't have to keep making new trucks to replace
 the lost ones at the frontline.
\end_layout

\begin_layout Itemize
Support halftrack: Available in the light vehicle yard and with its name
 varying by faction, it is the only land-based unit that can give supply
 without being deployed.
 To compensate for this, its range is the most limited of all ammo suppliers.
 Its use case is close infantry support (it has a machinegun) at start of
 game and following tanks, so that they are supplied when making a break-through.
\end_layout

\begin_layout Itemize
Landing ships: Tank landing ships and infantry ferries also supply ammo
 to units on the beach.
 
\end_layout

\begin_layout Subsubsection
Empty storage penalty
\end_layout

\begin_layout Standard
Beware that if your logistics in storage ever drops below 50, your infantry
 will recieve a 30% rate-of-fire penalty.
\end_layout

\begin_layout Subsubsection
Supply bonuses
\end_layout

\begin_layout Standard
While small arms do not use ammo, they do benefit from being in supply range.
 Currently, infantry within supply range gets a 30% rate-of-fire boost,
 which can be crucial to win a battle.
\end_layout

\begin_layout Section
Sight
\end_layout

\begin_layout Subsection
introduction
\end_layout

\begin_layout Standard
Knowing about the line of sight system in Spring: 1944 is very important,
 as some units are weak and have excellent sight, while others are powerful
 but can't see a thing.
 And a big gun is useless if it cannot see the enemy, right? I recommend
 always playing in the line-of-sight overlay, which you activate by pressing
 'l' (lowercase L) on the keyboard.
 Beware: tanks and other vehicles are pretty much blind, you always need
 to escort them with infantry! There is nothing more frustrating than your
 brand new 200mm front-armor 10000 command tank getting killed by a simple
 grenade! LoS explanation:
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename LOS_demo.jpg
	width 100line%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
A - plain line of sight, everything is visible, except "hidden" units (binocular
s, rus commisars, landmines, ...) B - Vehicle line of sight, in this range
 vehicles are visible C - ?? D - Air line of sight, this is where you spot
 planes if you don't have radar E - you can't see anything from here, except
 from the occasional artillery fire seismic signature.
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Scouting
\end_layout

\begin_layout Standard
There are two special-purpose scouting units available in Spring: 1944:
\end_layout

\begin_layout Itemize
Binoculars: While this unit may appear blind as if it were a vehicle, it
 has a special attack ability, which will reveal a part of the map close
 to the unit once you attack (not fight, attack) the map area.
 You can get this unit only from barracks and only in a squad with other
 units: mortars, snipers or an infantry gun team.
\end_layout

\begin_layout Itemize
Scout plane: This is the long-term intelligence gathering unit.
 Should I build tank destroyers or not? Send a scout plane and see if they
 are building a tank yard! This unit is available in the headquarters and
 the radar (airplane-building factory).
 There are two icons with the plane there.
 One is for building it and the other is for launching it.
 Once you request it, there is a 15-second delay before it spawns.
 If you return it safely back to base, you even get up to 650 command back,
 making the cost of knowing what to do be only 350 command.
 Isn't that cheap?
\end_layout

\begin_layout Section
Managing the Frontline
\end_layout

\begin_layout Subsection
Infantry formations
\end_layout

\begin_layout Standard
Remember to always make lines with your infantry so that they don't all
 get killed in one lucky cannon shot! Try to leave atleast two soldiers
 of space between two soldiers for this reason.
 Beware that spring currently likes to blob units together if they are moving
 over longer distances or non-flat terrain.
 Issue multiple line-move FIGHT orders in that case.
 When attacking, always use the FIGHT command.
 That way, your soldiers will automatically stop and engage the enemy, preventin
g them from running and not shooting (as what happens when you use the regular
 MOVE command).
 If it looks like you are going to lose the skirmish, it may be advantageous
 to retreat the force (and later regroup them with newly-built units) using
 regular move commands and to sacrifice those two-three soldiers while running
 away.
\end_layout

\begin_layout Subsection
Tank and mixed formations
\end_layout

\begin_layout Standard
Always keep tanks behind your infantry lines, they outrange the infantry's
 sight anyway and will only get hurt after being spotted by enemy infantry.
 Only move them forward if you are purposely tank rushing the enemy base
 (such as to kill their logistic storage).
 When you are ordering units that move at different speeds, use CTRL-fight
 to make the faster ones slow down.
\end_layout

\begin_layout Subsection
Attacking hardened frontline
\end_layout

\begin_layout Standard
If the enemy has "entrenched" himself and you do not have tanks or the enemy
 has anti-tank units, you will need to use support weaponry to dislodge
 them.
 Early in the game you should, if possible, always keep a squad of mortars
 and a scout (the binoculars guy) on the standby for this.
 Later you can also use regular artillery for this.
 Once you have sight, the artillery will lock on to the target very fast
 (some 10s) and become a deadly weapon.
 Or, incase you are playing germany or japan, you could use the german Nebelwerf
er (from upgraded towed gun yard - self propelled), russian Katyusha or
 200 mm mortar (regular barracks) to render an area completely void of enemy.
 Be aware though, that these units consume a lot of ammo for a single shot!
\end_layout

\end_body
\end_document
